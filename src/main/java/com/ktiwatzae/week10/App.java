package com.ktiwatzae.week10;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle cir = new Circle(2);
        System.out.println(cir);
        System.out.printf("%s Area: %.2f \n",cir.getName(),cir.calArea());
        System.out.printf("%s Perimeter: %.2f \n",cir.getName(),cir.calPerimeter());

        System.out.println("======================================");
        Circle cir2 = new Circle(3);
        System.out.println(cir2);
        System.out.printf("%s Area: %.2f \n",cir2.getName(),cir2.calArea());
        System.out.printf("%s Perimeter: %.2f \n",cir2.getName(),cir2.calPerimeter());
        System.out.println("======================================");

        Triangle tri = new Triangle(2, 2, 2);
        System.out.println(tri);
        System.out.printf("%s Area: %.2f \n",tri.getName(),tri.calArea());
        System.out.printf("%s Perimeter: %.2f \n",tri.getName(),tri.calPerimeter());

       

    }
}
