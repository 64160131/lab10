package com.ktiwatzae.week10;

public class Triangle extends Shape { 
    private double a;
    private double b;
    private double c;

    public Triangle(double a,double b,double c){
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calArea() {
        double s = (a + b + c) / 2;
        s = s * (s - a) * (s - b) * (s - c);
        s = Math.sqrt(s);
        return s;

    }

    @Override
    public double calPerimeter() {
        return a + b + c;
    }

    @Override
    public String toString() {
        return this.getName() + " A:"+this.a + " B:" + this.b + " C:" + this.c;

    }

    
}
